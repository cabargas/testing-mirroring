/*
 * Copyright 2014-2016 Hippo B.V. (http://www.onehippo.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
// Tests exist alongside the component they are testing with no separate test directory required; the build process
// should be sophisticated enough to handle this. via https://github.com/ngbp/ng-boilerplate#philosophy

module.exports = function (config) {

  var buildConfig = require('./build.config.js');

  config.set({
    // base path that will be used to resolve all patterns (eg. files, exclude)
    basePath: '',

    // frameworks to use
    // available frameworks: https://npmjs.org/browse/keyword/karma-adapter
    frameworks: ['jasmine'],

    // list of files / patterns to load in the browser
    files: [
      buildConfig.components_dir + '/jquery/dist/jquery.js',
      buildConfig.components_dir + '/chosen-npm/public/chosen.jquery.js',
      buildConfig.components_dir + '/google-code-prettify/src/prettify.js',
      buildConfig.components_dir + '/angular/angular.js',
      buildConfig.components_dir + '/angular-mocks/angular-mocks.js',
      buildConfig.components_dir + '/angular-route/angular-route.js',
      buildConfig.components_dir + '/angular-ui-bootstrap/ui-bootstrap-tpls.min.js',
      buildConfig.components_dir + '/angular-ui-tree/dist/angular-ui-tree.js',
      buildConfig.components_dir + '/angular-chosen-localytics/dist/angular-chosen.js',
      buildConfig.components_dir + '/angular-aria/angular-aria.js',
      buildConfig.mainjs,
      buildConfig.jstpl,
      buildConfig.src_dir + '/angularjs/**/*.js'
    ],

    // list of files to exclude
    exclude: [],

    // preprocess matching files before serving them to the browser
    // available preprocessors: https://npmjs.org/browse/keyword/karma-preprocessor
    preprocessors: {},

    // test results reporter to use
    // possible values: 'dots', 'progress'
    // available reporters: https://npmjs.org/browse/keyword/karma-reporter
    reporters: ['dots', 'junit'],

    junitReporter: {
      outputDir: './target/surefire-reports/',
      outputFile: 'TEST-karma-results.xml',
    },

    // web server port
    port: 9876,

    // enable / disable colors in the output (reporters and logs)
    colors: true,

    // level of logging
    // possible values: LOG_DISABLE || LOG_ERROR || LOG_WARN || LOG_INFO || LOG_DEBUG
    logLevel: config.LOG_INFO,

    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: false,

    // start these browsers
    // available browser launchers: https://npmjs.org/browse/keyword/karma-launcher
    browsers: ['PhantomJS'],

    // Continuous Integration mode
    // if true, Karma captures browsers, runs the tests and exits
    singleRun: false
  });
};
