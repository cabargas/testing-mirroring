Hippo Theme
===========

[![Build Status](https://travis-ci.org/onehippo/hippo-theme.png?branch=master)](https://travis-ci.org/onehippo/hippo-theme)

The Hippo theme is a centralised library containing reusable components for Hippo-related projects.
You can use it to create clickable mockups or end-projects that need to be in line with the Hippo styling.

## Development environment setup
#### Prerequisites

* [NodeJS](https://nodejs.org/) (NodeJS)
* [Git](http://git-scm.com/)

#### NPM Global Dependencies

* [grunt-cli](http://gruntjs.com/) (task automation)

#### Installation
Run the commands below in the project root directory.
#####1. Install Grunt

    $ sudo npm install -g grunt-cli

#####2. Install project dependencies

    $ npm install

## Useful commands

####Generate build
The build version is located in the `dist` directory.

    $ grunt build

####Run tests
The tests need to pass in order to build the demo.

    $ grunt test

####Setup server
The browser will show the demo website which shows all available components for the theme.

    $ grunt server

####Watch task for the Hippo theme during development
When working on end projects that use Hippo theme, sometimes changes need to be made to Hippo theme
and you want to see the results right away. For this we can use `npm link`.
First create a global link to the Hippo theme

    $ npm link

Assuming you have a watch task with livereload enabled running in your end project, you can use the
npmlink_watch task to watch the Hippo theme files (running two live-reload watch tasks in parallel is not possible)

    $ grunt npmlink_watch

Then in your end project run

    $ npm link hippo-theme

When the link is no longer necessary, simply remove it with

    $ npm uninstall hippo-theme

## Deployment to Nexus
#### Prerequisites

* [Maven](http://maven.apache.org/)

#### Deployment command

    $ mvn deploy

# Development
#### Adding icons
To add an icon to the theme and use it in the CMS Java code, perform the following steps:

* Add the .svg file to the src/images/icons folder
* Ensure that there is a copyright header
* Ensure that the icons does have color by setting 'fill="currentColor"' or 'stroke="currentColor"' where applicable
* Add the icon to the demo page
* Add the icon constant to the CMS project's Icon.java (api/src/main/java/org/hippoecm/frontend/skin/Icon.java)
