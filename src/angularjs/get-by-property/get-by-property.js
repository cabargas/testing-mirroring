/*
 * Copyright 2015 Hippo B.V. (http://www.onehippo.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  "use strict";

  angular.module('hippo.theme')
    .filter('hippoGetByProperty', function () {
      return function (collection, propertyName, propertyValue, subCollection) {
        var itemWithProperty;

        function findPropertiesAndSubProperties (newCollection) {
          for (var i = 0; i < newCollection.length; i++) {
            if (newCollection[i][propertyName] === propertyValue) {
              itemWithProperty = newCollection[i];
            }
            if (subCollection && newCollection[i][subCollection]) {
              findPropertiesAndSubProperties(newCollection[i][subCollection]);
            }
          }
        }

        findPropertiesAndSubProperties(collection);

        return itemWithProperty;
      };
    });
}());
