/*
 * Copyright 2015 Hippo B.V. (http://www.onehippo.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

describe('the get by property filter', function () {
  'use strict';

  var getByPropertyFilter;

  beforeEach(module('hippo.theme'));

  beforeEach(inject(function (_hippoGetByPropertyFilter_) {
    getByPropertyFilter = _hippoGetByPropertyFilter_;
  }));

  var items = [
      {
        id: 1,
        title: 'Item 1',
        items: [
          {
            id: 11,
            title: 'Item 1.1'
          }
        ]
      },
      {
        id: 2,
        title: 'Item 2',
        items: []
      }
    ],
    items1 = [
      {
        id: 1,
        title: 'Item 1',
        collapsed: true,
        items: [
          {
            id: 11,
            title: 'Item 1.1'
          }
        ]
      },
      {
        id: 2,
        title: 'Item 2',
        items: []
      }
    ];

  it('should find an object by property', function () {
    var foundItem = {
      id: 2,
      title: 'Item 2',
      items: []
    };
    expect(getByPropertyFilter(items, 'id', 2)).toEqual(foundItem);
  });

  it('should find an object by property in a sub item', function () {
    var foundItem = {
      id: 11,
      title: 'Item 1.1'
    };
    expect(getByPropertyFilter(items1, 'id', 11, 'items')).toEqual(foundItem);
  });

});
