/*
 * Copyright 2014-2015 Hippo B.V. (http://www.onehippo.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  angular.module('hippo.theme')

  /**
   * @ngdoc directive
   * @name hippoTree
   * @restrict A
   *
   * @description
   * Tree component for the Hippo Theme based on [NestedSortable](https://github.com/JimLiu/Angular-NestedSortable).
   *
   * @param {Array} items The items to use for the Tree. Each item is an object with `title` (String) and `items`
   *   (Array) property.
   * @param {Object} selectedItem The item that should be marked as selected.
   * @param {callbacks} callbacks The available callbacks. A list of all available callbacks is available at
   * the [Hippo Theme demo](http://onehippo.github.io/hippo-theme-demo/) page.
   */
    .directive('hippoTree', function () {
      return {
        restrict: 'A',
        transclude: true,
        scope: {
          drag: '=',
          options: '=callbacks',
          selectedItem: '=',
          treeItems: '=items'
        },
        templateUrl: 'tree/tree.tpl.html',
        controller: 'hippo.theme.tree.TreeCtrl'
      };
    })

    .controller('hippo.theme.tree.TreeCtrl', [
      '$transclude',
      '$scope',
      '$filter',
      function ($transclude, $scope, $filter) {
        this.renderTreeTemplate = $transclude;

        function collectNodes (items, map) {
          angular.forEach(items, function (item) {
            map[item.id] = item;
            collectNodes(item.items, map);
          });
          return map;
        }

        function copyCollapsedState (srcNodes, targetNodes) {
          angular.forEach(srcNodes, function (srcNode) {
            var targetNode = targetNodes[srcNode.id];
            if (targetNode) {
              targetNode.collapsed = srcNode.collapsed;
            }
          });
        }

        $scope.toggleItem = function () {
          var item = this.$modelValue;
          this.toggle();
          item.collapsed = !item.collapsed;

          if (item.collapsed && $filter('hippoGetByProperty')(item.items, 'id', $scope.selectedItem.id, 'items')) {
            $scope.selectItem(item);
          }
          if (angular.isFunction($scope.options.toggleItem)) {
            $scope.options.toggleItem(item);
          }
        };

        $scope.selectItem = function (item) {
          if (!item) {
            item = this.$modelValue;
          }
          $scope.selectedItem = item;
          if (angular.isFunction($scope.options.selectItem)) {
            $scope.options.selectItem(item);
          }
        };

        $scope.displayTreeItem = function (item) {
          if (angular.isFunction($scope.options.displayTreeItem)) {
            return $scope.options.displayTreeItem(item);
          } else {
            return true;
          }
        };

        $scope.$watch('treeItems', function (newItems, oldItems) {
          var oldNodes = collectNodes(oldItems, {}),
            newNodes = collectNodes(newItems, {});
          copyCollapsedState(oldNodes, newNodes);
        });
      }
    ])

    .directive('hippoTreeTemplate', function () {
      return {
        require: '^hippoTree',
        link: function (scope, element, attrs, controller) {
          controller.renderTreeTemplate(scope, function (dom) {
            element.replaceWith(dom);
          });
        }
      };
    })

    .directive('hippoNodeTemplateUrl', [
      '$compile',
      '$http',
      '$templateCache',
      function ($compile, $http, $templateCache) {
        return {
          restrict: 'A',
          link: function (scope, element, attr) {
            var templateUrl = attr.hippoNodeTemplateUrl;
            var innerTemplate = $templateCache.get(templateUrl);

            if (typeof innerTemplate === 'undefined') {
              $http.get(templateUrl).then(function (response) {
                innerTemplate = response.data;
                $templateCache.put(templateUrl, innerTemplate);

                $compile(innerTemplate)(scope, function (clone) {
                  element.append(clone);
                });
              });
            } else {
              $compile(innerTemplate)(scope, function (clone) {
                element.append(clone);
              });
            }
          }
        };
      }
    ]);
})();
