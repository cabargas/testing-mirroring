/*
 * Copyright 2014-2015 Hippo B.V. (http://www.onehippo.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  "use strict";

  angular.module('hippo.theme')

  /**
   * @ngdoc service
   * @name hippoViewportSizes
   *
   * @description
   * Holds the different possible viewport sizes.
   * It is able to return the current viewport size and provides a method to set the current viewport size.
   */
    .service('hippoViewportSizes', [
      function () {
        var viewportSizes = {};

        var sizes = [
          {
            order: 0,
            name: 'xs',
            active: false
          },

          {
            order: 1,
            name: 'sm',
            active: false
          },

          {
            order: 2,
            name: 'md',
            active: false
          },

          {
            order: 3,
            name: 'lg',
            active: false
          }
        ];

        /**
         * @ngdoc method
         * @name getAll
         * @methodOf hippoViewportSizes
         *
         * @description
         * Returns all the possible viewport sizes
         *
         * @returns {Array} List of viewport sizes
         */
        viewportSizes.getAll = function () {
          return sizes;
        };

        /**
         * @ngdoc method
         * @name setCurrent
         * @methodOf hippoViewportSizes
         * @param {Object} viewport The viewport to set as active
         *
         * @description
         * Sets the current active viewport. It also updates the $rootScope `activeViewport` property with the active
         *   viewport;
         */
        viewportSizes.setCurrent = function (viewport) {
          angular.forEach(sizes, function (size) {
            size.active = (viewport.name == size.name);
          });
        };

        /**
         * @ngdoc method
         * @name getCurrent
         * @methodOf hippoViewportSizes
         *
         * @description
         * Fetches the current active viewport
         *
         * @returns {Object} The current active viewport
         */
        viewportSizes.getCurrent = function () {
          for (var i = 0, len = sizes.length; i < len; i++) {
            if (sizes[i].active === true) {
              return sizes[i];
            }
          }
        };

        return viewportSizes;
      }
    ])

  /**
   * @ngdoc directive
   * @name hippoViewportTest
   * @restrict A
   * @requires $window
   *
   * @description
   * Detects the current active viewport by creating an empty div-element and attaching Bootstrap 3 classes to it.
   * When the created element is hidden, the related viewport for the class given is set to active.
   *
   * When the window gets resized, the possible new viewport will automatically be detected and set as active.
   */
    .directive('hippoViewportTest', [
      '$window',
      'hippoViewportSizes',
      function ($window, ViewportSizes) {
        return {
          restrict: 'A',
          replace: true,
          template: '<div></div>',
          link: function (scope, elem) {
            // initial detection
            detectViewportSize();

            // window resize
            angular.element($window).bind('resize', function () {
              detectViewportSize();
            });

            // detect viewport size
            function detectViewportSize () {
              // optimized version of http://stackoverflow.com/a/15150381/363448
              var emptyDiv = angular.element('<div>');
              elem.append(emptyDiv);

              var sizes = ViewportSizes.getAll();

              for (var i = sizes.length - 1; i >= 0; i--) {
                var size = sizes[i];

                emptyDiv.addClass('hidden-' + size.name);
                if (emptyDiv.is(':hidden')) {
                  emptyDiv.remove();
                  ViewportSizes.setCurrent(size);
                  return;
                }
              }
            }
          }
        };
      }
    ]);
}());
