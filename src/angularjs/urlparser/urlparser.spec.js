/*
 * Copyright 2014-2015 Hippo B.V. (http://www.onehippo.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
describe('The URL parser service', function () {
  'use strict';

  var urlParserService = null;

  beforeEach(module('hippo.theme'));

  beforeEach(inject([
    'hippo.theme.UrlParser',
    function (urlParser) {
      urlParserService = urlParser;

    }
  ]));

  it('should be defined', function () {
    expect(urlParserService).not.toBe(null);
  });

  it('should have a method getAll', function () {
    spyOn(urlParserService, 'getAll');
    urlParserService.getAll();
    expect(urlParserService.getAll).toHaveBeenCalled();
  });

  it('should have a method getFirst', function () {
    spyOn(urlParserService, 'getFirst');
    urlParserService.getFirst();
    expect(urlParserService.getFirst).toHaveBeenCalled();
  });
});
