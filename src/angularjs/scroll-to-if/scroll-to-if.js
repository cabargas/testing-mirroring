/*
 * Copyright 2014-2015 Hippo B.V. (http://www.onehippo.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  /**
   * @ngdoc directive
   * @name scrollToIf
   *
   * @description
   * Directive to scroll to an item if an expression evaluates to true.
   */
  angular.module('hippo.theme').directive('scrollToIf', [
    '$timeout',
    function ($timeout) {
      var getParentOfScrollItem = function (element) {
        element = element.parentElement;
        while (element) {
          if (element.scrollHeight !== element.clientHeight) {
            return element;
          }
          if (element.parentElement) {
            element = element.parentElement;
          } else {
            return element;
          }
        }
        return null;
      };
      return function (scope, element, attrs) {
        scope.$watch(attrs.scrollToIf, function (value) {
          if (value) {
            $timeout(function () {
              var parent = getParentOfScrollItem(element[0]),
                topPadding = parseInt(window.getComputedStyle(parent, null).getPropertyValue('padding-top')) || 0,
                leftPadding = parseInt(window.getComputedStyle(parent, null).getPropertyValue('padding-left')) || 0,
                elemOffsetTop = element[0].offsetTop,
                elemOffsetLeft = element[0].offsetLeft,
                elemHeight = element[0].clientHeight,
                elemWidth = element[0].clientWidth;

              if (elemOffsetTop < parent.scrollTop) {
                parent.scrollTop = elemOffsetTop + topPadding;
              } else if (elemOffsetTop + elemHeight > parent.scrollTop + parent.clientHeight) {
                if (elemHeight > parent.clientHeight) {
                  elemHeight = elemHeight - (elemHeight - parent.clientHeight);
                }
                parent.scrollTop = elemOffsetTop + topPadding + elemHeight - parent.clientHeight;
              }
              if (elemOffsetLeft + elemWidth > parent.scrollLeft + parent.clientWidth) {
                parent.scrollLeft = elemOffsetLeft + leftPadding;
              }
            }, 0);
          }
        });
      };
    }
  ]);
}());
