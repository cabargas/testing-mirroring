/*
 * Copyright 2014 Hippo B.V. (http://www.onehippo.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
(function () {
  'use strict';

  // Override ui.bootstrap carousel template
  angular.module('template/carousel/carousel.html', ['carousel/carousel.tpl.html']).run([
    '$templateCache',
    function ($templateCache) {
      var carouselTpl = $templateCache.get('carousel/carousel.tpl.html');
      $templateCache.put('template/carousel/carousel.html', carouselTpl);
    }
  ]);

  angular.module('hippo.theme').directive('hippoCarouselIndicator', [
      function () {
        return {
          restrict: 'A',
          templateUrl: 'carousel/carousel-indicator.tpl.html',
          link: function (scope, element, attr) {
            // Get corresponding slide object from the slide scope
            var slideObj = scope.slide.$element.scope().slides[scope.$index];
            scope.imgSrc = slideObj.image;
          }
        };
      }
    ]
  );
}());
