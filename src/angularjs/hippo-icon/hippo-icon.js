/*
 * Copyright 2015 Hippo B.V. (http://www.onehippo.com)
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

(function () {
  'use strict';

  angular.module('hippo.theme').directive('hippoIcon', [

    function () {
      return {
        replace: true,
        restrict: 'E',
        scope: {
          name: '@',
          position: '@',
          size: '@'
        },
        templateUrl: 'hippo-icon/hippo-icon.tpl.html',
        link: function (scope) {
          var xlink,
            iconPosition = '',
            iconName = '',
            defaultSize = 'm';

          if (scope.position) {
            angular.forEach(scope.position.split(' '), function (position, i) {
              if (position === 'center') {
                if (i === 0) {
                  position = 'vcenter';
                } else {
                  position = 'hcenter';
                }
              }

              iconPosition += ' hi-' + position;
            });
          }

          scope.$watchGroup(['name', 'size'], function () {
            if (scope.size) {
              iconName = ' hi-' + scope.name + ' hi-' + scope.size;
              xlink = '#hi-' + scope.name + '-' + scope.size;
            } else {
              iconName += ' hi-' + scope.name + ' hi-' + defaultSize;
              xlink = '#hi-' + scope.name + '-' + defaultSize;
            }

            scope.className = 'hi' + iconPosition + iconName;
            scope.xlink = xlink;
          });
        }
      };
    }
  ]);
})();